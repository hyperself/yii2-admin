<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $routes [] */

$this->title = Yii::t('rbac-admin', 'Routes');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Yii::t('rbac-admin', 'New route(s)'); ?></div>
        <div class="layui-card-body layui-row">
            <form class="layui-form" action="<?= Url::to(['create']) ?>" onsubmit="return false">
                <div class="layui-col-md11">
                    <input type="text" name="route" required lay-verify="required"
                           placeholder="<?= Yii::t('rbac-admin', 'New route(s)'); ?>" autocomplete="off"
                           class="layui-input">
                </div>
                <div class="layui-col-md1">
                    <button class="layui-btn" lay-submit lay-filter="submit"><?= Yii::t('rbac-admin', 'Add') ?></button>
                </div>
            </form>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">
            <?= Html::encode($this->title) ?>
            <a class="icon pear-icon pear-icon-refresh" lay-active="refresh" href="javascript:"></a>
        </div>
        <div class="layui-card-body">
            <div id="transfer"></div>
        </div>
    </div>
</div>
<script>
    <?php $this->beginBlock('script'); ?>
    layui.use(['transfer', 'util'], function () {
        var $ = layui.$,
            transfer = layui.transfer,
            util = layui.util,
            form = layui.form;

        transfer.render({
            id: 'transfer',
            elem: '#transfer',
            width: '47.5%',
            height: 500,
            title: [
                '<?= Yii::t('rbac-admin', 'Available'); ?>',
                '<?= Yii::t('rbac-admin', 'Assigned'); ?>'
            ],
            showSearch: true,
            data: <?=json_encode($routes['list'])?>,
            value: <?=json_encode($routes['assigned'])?>,
            onchange: function (obj, index) {
                var routes = [];
                $.each(obj, function (index, item) {
                    routes.push(item.value);
                });

                var href = ['<?=Url::to(['assign'])?>', '<?=Url::to(['remove'])?>'];
                $.post(href[index], {routes: routes}, function (res) {
                });
            }
        });

        //监听提交
        form.on('submit(submit)', function (data) {
            $.post(data.form.action, {route: data.field.route}, function (res) {
                transfer.reload('transfer', {
                    data: res.list,
                    value: res.assigned
                })
            });
            return false;
        });

        util.event('lay-active', {
            refresh: function (othis) {
                $.post('<?=Url::to(['refresh'])?>', function (res) {
                    transfer.reload('transfer', {
                        data: res.list,
                        value: res.assigned
                    })
                })
            }
        });
    })
    <?php $this->endBlock(); $this->registerjs($this->blocks['script']); ?>
</script>