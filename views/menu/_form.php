<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use CuiFox\admin\models\Menu;
use CuiFox\admin\assets\AutoCompleteAsset;

/* @var $this yii\web\View */
/* @var $model CuiFox\admin\models\Menu */
/* @var $form yii\widgets\ActiveForm */

AutoCompleteAsset::register($this);
$opts = Json::htmlEncode([
    'menus' => Menu::getMenuSource(),
    'routes' => Menu::getSavedRoutes(),
]);
$this->registerJs("var _opts = $opts;");
$this->registerJs($this->render('_script.js'));
?>
<style>
    .ui-autocomplete {
        z-index: 1000;
    }
</style>
<?php $form = ActiveForm::begin(['options' => ['class' => 'layui-form']]); ?>
<?= Html::activeHiddenInput($model, 'parent', ['id' => 'parent_id']); ?>
<?= $form->field($model, 'name', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['maxlength' => 128, 'class' => 'layui-input']) ?>
<?= $form->field($model, 'parent_name', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['id' => 'parent_name', 'class' => 'layui-input']) ?>
<?= $form->field($model, 'route', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['id' => 'route', 'class' => 'layui-input']) ?>
<?= $form->field($model, 'order', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->input('number', ['class' => 'layui-input']) ?>
<?= $form->field($model, 'data', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textarea(['rows' => 4, 'class' => 'layui-textarea']) ?>
<div class="layui-form-item">
    <div class="layui-input-block">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin',
            'Update'), [
            'class' => $model->isNewRecord ? 'layui-btn' : 'layui-btn layui-btn-normal'
        ])
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
