<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use CuiFox\admin\assets\StaticAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CuiFox\admin\models\searchs\Menu */

StaticAsset::register($this);

$this->title = Yii::t('rbac-admin', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create Menu'), ['create'], ['class' => 'layui-btn']) ?>
            </p>
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'layui-table'],
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['style' => 'width:20px;text-align:center;'],
                        'contentOptions' => ['style' => 'text-align:center;'],
                    ],
                    [
                        'attribute' => 'name',
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'attribute' => 'menuParent.name',
                        'filter' => Html::activeTextInput($searchModel, 'parent_name', [
                            'class' => 'layui-input',
                            'id' => null
                        ]),
                        'label' => Yii::t('rbac-admin', 'Parent'),
                    ],
                    [
                        'attribute' => 'route',
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'attribute' => 'order',
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['style' => 'width:110px;'],
                        'buttonOptions' => ['class' => 'layui-btn layui-btn-xs'],
                    ],
                ],
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
