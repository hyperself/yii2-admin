<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model CuiFox\admin\models\Menu */

$this->title = Yii::t('rbac-admin', 'Create Menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Menus'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <?= $this->render('_form', [
                'model' => $model,
            ])
            ?>
        </div>
    </div>
</div>
