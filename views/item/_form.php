<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use CuiFox\admin\components\RouteRule;
use CuiFox\admin\assets\AutoCompleteAsset;
use yii\helpers\Json;
use CuiFox\admin\components\Configs;

/* @var $this yii\web\View */
/* @var $model CuiFox\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $context CuiFox\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$rules = Configs::authManager()->getRules();
unset($rules[RouteRule::RULE_NAME]);
$source = Json::htmlEncode(array_keys($rules));

$js = <<<JS
    $('#rule_name').autocomplete({
        source: $source,
    });
JS;
AutoCompleteAsset::register($this);
$this->registerJs($js);
?>
<style>
    .ui-autocomplete {
        z-index: 1000;
    }
</style>
<?php $form = ActiveForm::begin(['id' => 'item-form', 'options' => ['class' => 'layui-form']]); ?>
<?= $form->field($model, 'name', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['maxlength' => 64, 'class' => 'layui-input']) ?>
<?= $form->field($model, 'description', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textarea(['rows' => 2, 'class' => 'layui-textarea']) ?>
<?= $form->field($model, 'ruleName', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['id' => 'rule_name', 'class' => 'layui-input']) ?>
<?= $form->field($model, 'data', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textarea(['rows' => 6, 'class' => 'layui-textarea']) ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin',
                'Update'), [
                'class' => $model->isNewRecord ? 'layui-btn' : 'layui-btn layui-btn-normal',
                'name' => 'submit-button'
            ])
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>