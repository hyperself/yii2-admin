<?php

use CuiFox\admin\components\ItemController;
use CuiFox\admin\models\AuthItem;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\DetailView;
use CuiFox\admin\assets\LayUIAsset;

/* @var $this View */
/* @var $model AuthItem */
/* @var $context ItemController */

LayUIAsset::register($this);

$context = $this->context;
$labels = $context->labels();
$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $labels['Items']), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$items = $model->transferItems();
?>

<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Update'), ['update', 'id' => $model->name],
                    ['class' => 'layui-btn']); ?>
                <?= Html::a(Yii::t('rbac-admin', 'Delete'), ['delete', 'id' => $model->name], [
                    'class' => 'layui-btn',
                    'data-confirm' => Yii::t('rbac-admin', 'Are you sure to delete this item?'),
                    'data-method' => 'post',
                ]);
                ?>
                <?= Html::a(Yii::t('rbac-admin', 'Create'), ['create'], ['class' => 'layui-btn']); ?>
            </p>
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => 'layui-table'],
                'attributes' => [
                    'name',
                    'description:ntext',
                    'ruleName',
                    'data:ntext',
                ],
                'template' => '<tr><th style="width:25%">{label}</th><td>{value}</td></tr>',
            ]);
            ?>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header"><?= Yii::t('rbac-admin', 'Assigned users'); ?></div>
        <div class="layui-card-body">
            <table id="users" class="layui-hide" lay-filter="list"></table>
            <script type="text/html" id="toolbar-right">
                <a class="layui-btn layui-btn-xs" lay-event="view">查看</a>
            </script>
        </div>
    </div>
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><?= Yii::t('rbac-admin', 'Routes'); ?></div>
                <div class="layui-card-body">
                    <div id="transfer-route"></div>
                </div>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><?= Yii::t('rbac-admin', 'Permission'); ?></div>
                <div class="layui-card-body">
                    <div id="transfer-permission"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    <?php $this->beginBlock('script'); ?>
    layui.use(['transfer', 'table'], function () {
        var $ = layui.$,
            table = layui.table,
            transfer = layui.transfer;

        transfer.render({
            id: 'transfer-route',
            elem: '#transfer-route',
            width: '44.8%',
            height: 500,
            title: [
                '<?= Yii::t('rbac-admin', 'Available'); ?>',
                '<?= Yii::t('rbac-admin', 'Assigned'); ?>'
            ],
            data: <?=json_encode($items['list']['route'] ?? [])?>,
            value: <?=json_encode($items['assigned']['route'] ?? [])?>,
            showSearch: true,
            onchange: function (obj, index) {
                var items = [];
                $.each(obj, function (index, item) {
                    items.push(item.value);
                });

                var href = [
                    '<?=Url::to(['assign', 'id' => $model->name])?>',
                    '<?=Url::to(['remove', 'id' => $model->name])?>'
                ];
                $.post(href[index], {items: items}, function (res) {
                });
            }
        });

        transfer.render({
            id: 'transfer-permission',
            elem: '#transfer-permission',
            width: '44.8%',
            height: 500,
            title: [
                '<?= Yii::t('rbac-admin', 'Available'); ?>',
                '<?= Yii::t('rbac-admin', 'Assigned'); ?>'
            ],
            data: <?=json_encode($items['list']['permission'] ?? [])?>,
            value: <?=json_encode($items['assigned']['permission'] ?? [])?>,
            showSearch: true,
            onchange: function (obj, index) {
                var items = [];
                $.each(obj, function (index, item) {
                    items.push(item.value);
                });

                var href = [
                    '<?=Url::to(['assign', 'id' => $model->name])?>',
                    '<?=Url::to(['remove', 'id' => $model->name])?>'
                ];
                $.post(href[index], {items: items}, function (res) {
                });
            }
        });

        table.render({
            id: 'users',
            elem: '#users',
            url: '<?=Url::to(['get-users', 'id' => $model->name])?>',
            page: true,
            height: 200,
            response: {
                statusName: 'code',
                statusCode: 1
            },
            parseData: function (res) {
                return {
                    code: res.code,
                    count: res.data.count,
                    data: res.data.list
                };
            },
            cols: [[
                {title: '用户名', field: 'username'},
                {title: '操作', toolbar: '#toolbar-right', align: "center", width: 120},
            ]]
        });

        // 工具列点击事件
        table.on('tool(list)', function (obj) {
            switch (obj.event) {
                case 'view':
                    window.location = obj.data.link
                    break;
            }
        });
    });
    <?php $this->endBlock(); $this->registerjs($this->blocks['script']); ?>
</script>