<?php

use yii\helpers\Html;
use CuiFox\admin\assets\LayUIAsset;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

LayUIAsset::register($this);

$controller = $this->context;
$menus = $controller->module->menus;
$route = $controller->route;
foreach ($menus as $i => $menu) {
    $menus[$i]['active'] = strpos($route, trim($menu['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $menus;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo layui-hide-xs layui-bg-black">RABC</div>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item layui-hide layui-show-md-inline-block">
                <a href="javascript:;">
                    <img src="//tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg"
                         class="layui-nav-img">
                    tester
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">Your Profile</a></dd>
                    <dd><a href="">Settings</a></dd>
                    <dd><a href="">Sign out</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item" lay-header-event="menuRight" lay-unselect>
                <a href="javascript:;">
                    <i class="layui-icon layui-icon-more-vertical"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">Menu</a>
                    <dl class="layui-nav-child">
                        <?php foreach ($this->params['nav-items'] as $it): ?>
                            <dd class="<?php if ($it['active']): ?>layui-this<?php endif; ?>"><a
                                        href="<?= Url::to($it['url']) ?>"><?= $it['label'] ?></a></dd>
                        <?php endforeach; ?>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <!-- 内容主体区域 -->
    <div class="layui-body layui-col-space10">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
            <legend>
                <?= Breadcrumbs::widget([
                    'options' => ['class' => 'layui-breadcrumb'],
                    'itemTemplate' => "{link}\n",
                    'activeItemTemplate' => "<a><cite>{link}<cite></a>\n",
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </legend>
        </fieldset>
        <?= $content ?>
    </div>
    <!-- 底部固定区域 -->
    <div class="layui-footer">
        底部固定区域
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    //JS
    layui.use(['element', 'layer', 'util'], function () {
        var element = layui.element,
            layer = layui.layer,
            util = layui.util,
            $ = layui.$;

        //头部事件
        util.event('lay-header-event', {
            //左侧菜单事件
            menuLeft: function (othis) {
                layer.msg('展开左侧菜单的操作', {icon: 0});
            },
            menuRight: function () {
                layer.open({
                    type: 1,
                    content: '<div style="padding: 15px;">处理右侧面板的操作</div>',
                    area: ['260px', '100%'],
                    offset: 'rt',//右上角
                    anim: 5,
                    shadeClose: true
                });
            }
        });

    });
</script>
