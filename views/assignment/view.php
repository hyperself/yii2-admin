<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model CuiFox\admin\models\Assignment */
/* @var $fullnameField string */

$userName = $model->{$usernameField};
if (!empty($fullnameField)) {
    $userName .= ' (' . ArrayHelper::getValue($model, $fullnameField) . ')';
}
$userName = Html::encode($userName);

$this->title = Yii::t('rbac-admin', 'Assignment') . ' : ' . $userName;

$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'Assignments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $userName;

$items = $model->transferItems();
?>
<div class="layui-col-md12">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><?= Yii::t('rbac-admin', 'Permission'); ?></div>
                <div class="layui-card-body">
                    <div id="transfer-permission"></div>
                </div>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-card">
                <div class="layui-card-header"><?= Yii::t('rbac-admin', 'Roles'); ?></div>
                <div class="layui-card-body">
                    <div id="transfer-role"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    <?php $this->beginBlock('script'); ?>
    layui.use(['transfer', 'table'], function () {
        var $ = layui.$,
            table = layui.table,
            transfer = layui.transfer;

        transfer.render({
            id: 'transfer-permission',
            elem: '#transfer-permission',
            width: '44.8%',
            height: 500,
            title: [
                '<?= Yii::t('rbac-admin', 'Available'); ?>',
                '<?= Yii::t('rbac-admin', 'Assigned'); ?>'
            ],
            data: <?=json_encode($items['list']['permission'] ?? [])?>,
            value: <?=json_encode($items['assigned']['permission'] ?? [])?>,
            showSearch: true,
            onchange: function (obj, index) {
                var items = [];
                $.each(obj, function (index, item) {
                    items.push(item.value);
                });

                var href = [
                    '<?=Url::to(['assign', 'id' => (string)$model->id])?>',
                    '<?=Url::to(['revoke', 'id' => (string)$model->id])?>'
                ];
                $.post(href[index], {items: items}, function (res) {
                });
            }
        });

        transfer.render({
            id: 'transfer-role',
            elem: '#transfer-role',
            width: '44.8%',
            height: 500,
            title: [
                '<?= Yii::t('rbac-admin', 'Available'); ?>',
                '<?= Yii::t('rbac-admin', 'Assigned'); ?>'
            ],
            data: <?=json_encode($items['list']['role'] ?? [])?>,
            value: <?=json_encode($items['assigned']['role'] ?? [])?>,
            showSearch: true,
            onchange: function (obj, index) {
                var items = [];
                $.each(obj, function (index, item) {
                    items.push(item.value);
                });

                var href = [
                    '<?=Url::to(['assign', 'id' => (string)$model->id])?>',
                    '<?=Url::to(['revoke', 'id' => (string)$model->id])?>'
                ];
                $.post(href[index], {items: items}, function (res) {
                });
            }
        });
    });
    <?php $this->endBlock(); $this->registerjs($this->blocks['script']); ?>
</script>