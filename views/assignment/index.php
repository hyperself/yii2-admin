<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CuiFox\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('rbac-admin', 'Assignments');
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'headerOptions' => ['style' => 'width:20px;text-align:center;'],
        'contentOptions' => ['style' => 'text-align:center;'],
    ],
    [
        'attribute' => $usernameField,
        'filterInputOptions' => ['class' => 'layui-input']
    ],
];
if (!empty($extraColumns)) {
    $columns = array_merge($columns, $extraColumns);
}
$columns[] = [
    'class' => 'yii\grid\ActionColumn',
    'template' => '{view}',
    'headerOptions' => ['style' => 'width:40px;'],
    'buttonOptions' => ['class' => 'layui-btn layui-btn-xs'],
];
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'layui-table'],
                'columns' => $columns,
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>