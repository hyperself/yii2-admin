<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \CuiFox\admin\models\form\Login */

$this->title = Yii::t('rbac-admin', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>Please fill out the following fields to login:</p>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'layui-form']]); ?>
            <?= $form->field($model, 'username', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->textInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'password', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->passwordInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'rememberMe', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>'
            ])->checkbox(['lay-skin' => 'primary']) ?>
            <div style="color:#999;margin:1em 0">
                If you forgot your password you can <?= Html::a('reset it', ['user/request-password-reset']) ?>.
                For new user you can <?= Html::a('sign up', ['user/sign-up']) ?>.
            </div>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <?= Html::submitButton(Yii::t('rbac-admin', 'Login'), [
                        'class' => 'layui-btn',
                        'name' => 'login-button'
                    ]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>