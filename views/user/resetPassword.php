<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \CuiFox\admin\models\form\ResetPassword */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>Please choose your new password:</p>
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'options' => ['class' => 'layui-form']]); ?>
            <?= $form->field($model, 'password', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->passwordInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'retypePassword', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->passwordInput(['class' => 'layui-input']) ?>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <?= Html::submitButton(Yii::t('rbac-admin', 'Save'), ['class' => 'layui-btn']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>