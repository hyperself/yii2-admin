<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \CuiFox\admin\models\form\PasswordResetRequest */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>Please fill out your email. A link to reset password will be sent there.</p>
            <?php $form = ActiveForm::begin([
                'id' => 'request-password-reset-form',
                'options' => [
                    'class' => 'layui-form'
                ]
            ]); ?>
            <?= $form->field($model, 'email', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->textInput(['class' => 'layui-input']) ?>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <?= Html::submitButton(Yii::t('rbac-admin', 'Send'), ['class' => 'layui-btn']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>