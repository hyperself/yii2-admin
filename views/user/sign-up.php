<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model \CuiFox\admin\models\form\Signup */

$this->title = Yii::t('rbac-admin', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>Please fill out the following fields to signup:</p>
            <?= Html::errorSummary($model) ?>
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'options' => ['class' => 'layui-form']]); ?>
            <?= $form->field($model, 'username', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->textInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'email', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->textInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'password', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->passwordInput(['class' => 'layui-input']) ?>
            <?= $form->field($model, 'retypePassword', [
                'options' => ['class' => 'layui-form-item'],
                'labelOptions' => ['class' => 'layui-form-label'],
                'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
            ])->passwordInput(['class' => 'layui-input']) ?>
            <div class="layui-form-item">
                <div class="layui-input-block">
                    <?= Html::submitButton(Yii::t('rbac-admin', 'Signup'), [
                        'class' => 'layui-btn',
                        'name' => 'signup-button'
                    ]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>