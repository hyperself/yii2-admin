<?php

use yii\helpers\Html;
use yii\grid\GridView;
use CuiFox\admin\components\Helper;
use CuiFox\admin\assets\StaticAsset;

/* @var $this yii\web\View */
/* @var $searchModel CuiFox\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

StaticAsset::register($this);

$this->title = Yii::t('rbac-admin', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'layui-table'],
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['style' => 'width:20px;text-align:center;'],
                        'contentOptions' => ['style' => 'text-align:center;'],
                    ],
                    [
                        'attribute' => 'username',
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'attribute' => 'email',
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return $model->status == 0 ? 'Inactive' : 'Active';
                        },
                        'filter' => [
                            0 => 'Inactive',
                            10 => 'Active'
                        ],
                        'filterInputOptions' => ['class' => 'layui-input'],
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => Helper::filterActionColumn(['view', 'activate', 'delete']),
                        'buttons' => [
                            'activate' => function ($url, $model) {
                                if ($model->status == 10) {
                                    return '';
                                }
                                $options = [
                                    'title' => Yii::t('rbac-admin', 'Activate'),
                                    'aria-label' => Yii::t('rbac-admin', 'Activate'),
                                    'data-confirm' => Yii::t('rbac-admin',
                                        'Are you sure you want to activate this user?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ];
                                return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                            }
                        ]
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
