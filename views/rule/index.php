<?php

use yii\helpers\Html;
use yii\grid\GridView;
use CuiFox\admin\assets\StaticAsset;

/* @var $this  yii\web\View */
/* @var $model CuiFox\admin\models\BizRule */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel CuiFox\admin\models\searchs\BizRule */

StaticAsset::register($this);

$this->title = Yii::t('rbac-admin', 'Rules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layui-col-md12">
    <div class="layui-card">
        <div class="layui-card-header"><?= Html::encode($this->title) ?></div>
        <div class="layui-card-body">
            <p>
                <?= Html::a(Yii::t('rbac-admin', 'Create Rule'), ['create'], ['class' => 'layui-btn']) ?>
            </p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => ['class' => 'layui-table'],
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'headerOptions' => ['style' => 'width:20px;text-align:center;'],
                        'contentOptions' => ['style' => 'text-align:center;'],
                    ],
                    [
                        'attribute' => 'name',
                        'label' => Yii::t('rbac-admin', 'Name'),
                        'filterInputOptions' => ['class' => 'layui-input']
                    ],
                    ['class' => 'yii\grid\ActionColumn',],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
