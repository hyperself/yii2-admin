<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this  yii\web\View */
/* @var $model CuiFox\admin\models\BizRule */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'layui-form']]); ?>
<?= $form->field($model, 'name', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['maxlength' => 64, 'class' => 'layui-input']) ?>
<?= $form->field($model, 'className', [
    'options' => ['class' => 'layui-form-item'],
    'labelOptions' => ['class' => 'layui-form-label'],
    'template' => '{label}<div class="layui-input-block">{input}{error}</div>',
])->textInput(['class' => 'layui-input']) ?>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('rbac-admin', 'Create') : Yii::t('rbac-admin',
                'Update'), [
                'class' => $model->isNewRecord ? 'layui-btn' : 'layui-btn layui-btn-normal'
            ])
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>