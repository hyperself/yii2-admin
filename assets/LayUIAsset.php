<?php

namespace CuiFox\admin\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class LayUIAsset
 * @package CuiFox\admin
 */
class LayUIAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@CuiFox/admin/sources/layui';
    /**
     * @inheritdoc
     */
    public $css = [
        'css/layui.css',
    ];
    /**
     * @inheritdoc
     */
    public $js = [
        'layui.js'
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        JqueryAsset::class
    ];
}
