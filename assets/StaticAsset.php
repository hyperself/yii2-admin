<?php

namespace CuiFox\admin\assets;

use yii\web\AssetBundle;

/**
 * Class StaticAsset
 * @package CuiFox\admin\assets
 */
class StaticAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@CuiFox/admin/sources/static';

    /**
     * @inheritdoc
     */
    public $css = [
        'style.css',
    ];
}