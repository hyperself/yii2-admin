<?php

namespace CuiFox\admin\assets;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class AutoCompleteAsset
 * @package CuiFox\admin\assets
 */
class AutoCompleteAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@bower/jquery-ui.autocomplete';

    /**
     * @inheritdoc
     */
    public $css = [
        'jquery-ui.custom.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'jquery-ui.autocomplete.min.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        JqueryAsset::class
    ];
}